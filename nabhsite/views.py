from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect

# Create your views here.
def profile(request):
    return render(request, 'profil.html')

def facts(request):
    return render(request, 'facts.html')