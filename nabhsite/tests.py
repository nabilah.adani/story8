from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profile
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class story8UnitTest(TestCase):
    def test_nabhsite_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_nabhsite_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)

    def test_nabhsite_using_profile_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'profil.html')
    
    def test_nabhsite_facts_url_is_exist(self):
        response = Client().get('/facts/')
        self.assertEqual(response.status_code, 200)

    def test_nabhsite_using_facts_html(self):
        response = Client().get('/facts/')
        self.assertTemplateUsed(response, 'facts.html')

    def test_nabhsite_content_is_exist(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn("Halo! Inilah Saya:", html_response)

    def test_nabhsite_content1_is_exist(self):
        response = Client().get('/facts/')
        html_response = response.content.decode('utf8')
        self.assertIn("Fakta Saya", html_response)


class story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story8FunctionalTest, self).tearDown()

    