from django.urls import path
from .views import *

urlpatterns = [
    path('', profile, name='profile'),
    path('facts/', facts , name='facts'),
]